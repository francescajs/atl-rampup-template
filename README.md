# Atlassian Ramp up Project

## Requirements

Make sure that you have Go and and npm installed in your computer.

## Files

- `main.go`: Sets up the connection to backend server
- `router.go`: Define your API here. Right now, if you make a call to `localhost:8080/`, then you will fetch a JSON object with the URL for the Codebase logo.
- `models.go`: Define appropriate structs here.
- `App.js`: the main file for the frontend. Start building your components here!

## Running the application

- Open two terminals�one of them is in the `/server` directory and the other is in `/frontend`.
- In `/server`, run `go run main.go` to start the server.
  _ If this is your first time, run `go build` to download all dependencies.
  _ You may need to run `go get <some github.com url>` to fetch a library
- In `/frontend`, run `npm start` to start the React app
  - If this is your first time, run `npm install` to download all dependencies. \* Upon success, you will be redirected to `localhost:3000/`

## Resources

- I referenced [this](https://levelup.gitconnected.com/build-a-todo-app-in-golang-mongodb-and-react-e1357b4690a6) article for setting up the repo.
- [S3 SDK for Go](https://docs.aws.amazon.com/sdk-for-go/api/service/s3/)
- [Getting started with React](https://reactjs.org/docs/getting-started.html)
- [Atlaskit](https://atlaskit.atlassian.com/)
