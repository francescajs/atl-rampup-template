import React from "react";
import "./App.css";
import HelloWorld from "./helloworld.react";

function App() {
  return <HelloWorld />;
}

export default App;
